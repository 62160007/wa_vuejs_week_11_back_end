const { ROLE } = require('../constant.js')
const mongoose = require('mongoose')
const { Schema } = mongoose
const userSchema = Schema({
  username: String,
  password: String, // paintext
  roles: {
    type: [String],
    default: [ROLE]
  }
})

module.exports = mongoose.model('User', userSchema)
